#!/bin/bash
set -x

#Variables:
INSTANCE_ID=$1
NEW_INSTANCE_TYPE=$2


#Stopping the ec2 instance:
echo "Stopping EC2 Instances $INSTANCE_ID"
aws ec2 stop-instances --instance-ids $INSTANCE_ID

# Wait until the instance is stopped
echo "Waiting for EC2 instance $INSTANCE_ID to stop..."
aws ec2 wait instance-stopped --instance-ids $INSTANCE_ID 

# Change the instance type
echo "Changing instance type of $INSTANCE_ID to $NEW_INSTANCE_TYPE..."
aws ec2 modify-instance-attribute --instance-id $INSTANCE_ID --instance-type "{\"Value\":\"$NEW_INSTANCE_TYPE\"}"

# Start the instance
echo "Starting EC2 instance $INSTANCE_ID..."
aws ec2 start-instances --instance-ids $INSTANCE_ID

# Wait until the instance is running
echo "Waiting for EC2 instance $INSTANCE_ID to start..."
aws ec2 wait instance-running --instance-ids $INSTANCE_ID

echo "EC2 instance $INSTANCE_ID has been updated and is now running."
