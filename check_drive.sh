#!/bin/bash

diskname=$(sudo lsblk | grep "10G" | awk '{print $1}')

####Check if the 10GB drive is not found: 
if [  "$diskname" = "" ]; then 
  echo "Error: 10GB drive is not found. Please check if the disk is mounted." 
  exit 1
fi 


