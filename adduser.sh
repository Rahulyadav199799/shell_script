#!/bin/bash

# Ensure the script exits on errors
set -e

# Check if the username is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <username>"
  exit 1
fi

# Variables
USERNAME=$1

# Add user
if id "$USERNAME" &>/dev/null; then
  echo "User $USERNAME already exists."
else
  useradd -m "$USERNAME" -s /bin/bash
  echo "User $USERNAME added successfully."
fi

# Grant the user sudo access
if ! grep -q "^$USERNAME" /etc/sudoers; then
  echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
  echo "Sudo privileges granted to $USERNAME."
else
  echo "Sudo privileges for $USERNAME already exist."
fi

