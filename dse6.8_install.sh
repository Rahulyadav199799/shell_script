#!/bin/bash

echo "############################################################################################"
echo "#################################### Installing NTP:########################################"
sudo apt install ntp -y
echo "#################################### NTP INSTALLED ########################################"
echo "#################################### NTP SERVICE STARTING ########################################"
sudo service ntp start -x
echo "#################################### NTP SERVICE STARTED ########################################"

echo "#################################### UPDATING SYSCTL CONFIG ########################################"
cat <<EOF | sudo tee -a  /etc/sysctl.conf
vm.max_map_count = 1048575
net.ipv4.tcp_keepalive_time=60
net.ipv4.tcp_keepalive_probes=3
net.ipv4.tcp_keepalive_intvl=10
net.core.rmem_max=16777216
net.core.wmem_max=16777216
net.core.rmem_default=16777216
net.core.wmem_default=16777216
net.core.optmem_max=40960
net.ipv4.tcp_rmem=4096 87380 16777216
net.ipv4.tcp_wmem=4096 65536 16777216
EOF

echo "#################################### UPDATED SYSCTL CONFIG ########################################" 

echo "#################################### RELOADING SYSCTL CONFIG ########################################"
sudo sysctl -p /etc/sysctl.conf
echo "#################################### RELOADED SYSCTL CONFIG ########################################"

echo "#################################### DISABLE ZONE RECLAIM ########################################"
echo 0 > /proc/sys/vm/zone_reclaim_mode
echo "#################################### DISABLED ZONE RECLAIM ########################################"

echo "#################################### DISABLE SWAP #################################################"
sudo sed -i '/^\/swap.img/s/^/#/' /etc/fstab

sudo swapoff -a
echo "#################################### DISABLED SWAP #################################################"

echo "#################################### GETTING DEVICE NAME #################################################"
device_name=$(lsblk -d -o NAME | grep -vE 'loop|NAME' | head -n 1)

# Check if we got a device name
if [[ -z "$device_name" ]]; then
  echo "Error: No device found. Exiting."
  exit 1
fi

echo "Your device name is $device_name."

echo "#################################### ADDING DEADLINE #################################################"
echo deadline | sudo tee /sys/block/$device_name/queue/scheduler > /dev/null
echo "#################################### DEADLINE IS SET #################################################"

echo "#################################### OPTIMIZE SPINNING DISK #################################################"
echo "Check to ensure read-ahead value is not set to 65536:"
sudo blockdev --report /dev/$device_name

echo "Set the readahead to 128, which is the recommended value:"
sudo blockdev --setra 128 /dev/$device_name
echo "################################################################################################################"
echo "###################################### INSTALLED PRE CASSANDRA PRODUCTION SETTINGS ##############################"
echo "################################################################################################################"


echo "################################################################################################################"
echo "###################################### INSTALLING DSE 6.8.49 ####################################################"
echo "################################################################################################################"

# Variables
dse_version=6.8.49
sleep 20
echo "#################################### INSTALLING JAVA 8 ########################################################"
echo "Downloading the java package....."
sudo apt-get install openjdk-8-jdk -y

echo "#################################### INSTALLED JAVA 11 ########################################################"

echo "#################################### CHECK JAVA VERSION ########################################################"
###To Check version:
java -version

echo "#################################### INSTALLING LIBAIO1 PACKAGES ########################################################"
###Install the libaio package:
sudo apt-get install libaio1
echo "#################################### INSTALLED LIBAIO1 PACKAGES ########################################################"

echo "#################################### ADDING DATASTAX REPOSITORY ########################################################"
###Add a DataStax repository file:
echo "deb https://debian.datastax.com/enterprise/ stable main" | sudo tee -a /etc/apt/sources.list.d/datastax.sources.list
echo "#################################### ADDED DATASTAX REPOSITORY ########################################################"

echo "####################################  DATASTAX REPOSITORY KEYS ADD ########################################################"
###Add the DataStax repository key:
curl -L https://debian.datastax.com/debian/repo_key | sudo apt-key add -
echo "#################################### ADDED DATASTAX REPOSITORY KEYS ADD ########################################################"

echo "#################################### UPDATING THE PACAKAGES ########################################################"
###Update the packages:
sudo apt-get update
echo "#################################### PACKAGES ARE UPDATED ########################################################"

echo "#################################### INSTALLING DSE 6.8.49 VERSION ########################################################"
####Install an earlier 6.9.x version:
sudo apt-get install dse=$dse_version-1 \
    dse-full=$dse_version-1 \
    dse-libcassandra=$dse_version-1 \
    dse-libgraph=$dse_version-1 \
    dse-libhadoop2-client-native=$dse_version-1 \
    dse-libhadoop2-client=$dse_version-1 \
    dse-liblog4j=$dse_version-1 \
    dse-libsolr=$dse_version-1 \
    dse-libspark=$dse_version-1 \
    dse-libtomcat=$dse_version-1
echo "#################################### INSTALLED DSE 6.8.49 VERSION ########################################################"

echo "#################################### CHECK DSE VERSION ########################################################"
dse -v

echo "################################################################################################################"
echo "###################################### DSE 6.8.49 IS INSTALLED ####################################################"
echo "################################################################################################################"
