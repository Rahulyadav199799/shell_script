#!/bin/bash

echo "############################################################################################"
echo "#################################### Installing NTP:########################################"
sudo apt-get install ntpdate
echo "#################################### NTP INSTALLED ########################################"
echo "#################################### NTP SERVICE STARTING ########################################"
sudo service ntp start -x
echo "#################################### NTP SERVICE STARTED ########################################"

echo "#################################### UPDATING SYSCTL CONFIG ########################################"
cat <<EOF | sudo tee -a  /etc/sysctl.conf
vm.max_map_count = 1048575
net.ipv4.tcp_keepalive_time=60
net.ipv4.tcp_keepalive_probes=3
net.ipv4.tcp_keepalive_intvl=10
net.core.rmem_max=16777216
net.core.wmem_max=16777216
net.core.rmem_default=16777216
net.core.wmem_default=16777216
net.core.optmem_max=40960
net.ipv4.tcp_rmem=4096 87380 16777216
net.ipv4.tcp_wmem=4096 65536 16777216
EOF

echo "#################################### UPDATED SYSCTL CONFIG ########################################" 

echo "#################################### RELOADING SYSCTL CONFIG ########################################"
sudo sysctl -p /etc/sysctl.conf
echo "#################################### RELOADED SYSCTL CONFIG ########################################"

echo "#################################### DISABLE ZONE RECLAIM ########################################"
echo 0 > /proc/sys/vm/zone_reclaim_mode
echo "#################################### DISABLED ZONE RECLAIM ########################################"

echo "#################################### DISABLE SWAP #################################################"
sudo sed -i ‘/\tswap\t/d’ /etc/fstab
echo "#################################### DISABLED SWAP #################################################"

echo "#################################### GETTING DEVICE NAME #################################################"
device_name=$(lsblk -d -o NAME | grep -vE 'loop|NAME' | head -n 1)

# Check if we got a device name
if [[ -z "$device_name" ]]; then
  echo "Error: No device found. Exiting."
  exit 1
fi

echo "Your device name is $device_name."

echo "#################################### ADDING DEADLINE #################################################"
echo deadline | sudo tee /sys/block/$device_name/queue/scheduler > /dev/null
echo "#################################### DEADLINE IS SET #################################################"

echo "#################################### OPTIMIZE SPINNING DISK #################################################"
echo "Check to ensure read-ahead value is not set to 65536:"
sudo blockdev --report /dev/$device_name

echo "Set the readahead to 128, which is the recommended value:"
sudo blockdev --setra 128 /dev/$device_name
echo "################################################################################################################"
echo "###################################### INSTALLED PRE CASSANDRA PRODUCTION SETTINGS ##############################"
echo "################################################################################################################"

